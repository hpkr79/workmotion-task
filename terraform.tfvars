source_code_dir        = "source_code"
global_name            = "simpleLambdaApiExample"
assume_role_policy     = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
handler                = "main.lambda_handler"
memory_size            = "128"
runtime                = "python3.8"
timeout                = "5"
publish                = true
endpoint_configuration = ["REGIONAL"]