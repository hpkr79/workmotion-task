variable "source_code_dir" {
  type = string
}
variable "global_name" {
  type = string
}
variable "assume_role_policy" {
  type = string
}
variable "handler" {
  type = string
}
variable "memory_size" {
  type = string
}
variable "runtime" {
  type = string
}
variable "timeout" {
  type = string
}
variable "publish" {
  type = bool
}
variable "endpoint_configuration" {
  type = list(any)
}