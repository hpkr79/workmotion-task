resource "aws_s3_bucket" "this" {
  bucket = lower(var.global_name)
  acl    = "private"
}

data "archive_file" "this" {
  type        = "zip"
  source_dir  = var.source_code_dir
  output_path = "code.zip"
}

resource "aws_s3_bucket_object" "this" {
  bucket = aws_s3_bucket.this.bucket
  key    = "${var.global_name}.zip"
  source = data.archive_file.this.output_path
  etag   = filemd5(data.archive_file.this.output_path)
}

resource "aws_iam_role" "this" {
  name               = var.global_name
  assume_role_policy = var.assume_role_policy
  path               = "/"
}

resource "aws_iam_role_policy_attachment" "AWSLambdaBasicExecutionRole_Policy" {
  role       = aws_iam_role.this.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
}

resource "aws_lambda_function" "this" {
  s3_bucket         = aws_s3_bucket.this.bucket
  s3_key            = aws_s3_bucket_object.this.key
  s3_object_version = aws_s3_bucket_object.this.version_id
  function_name     = var.global_name
  handler           = var.handler
  role              = aws_iam_role.this.arn
  memory_size       = var.memory_size
  runtime           = var.runtime
  timeout           = var.timeout
  publish           = var.publish
  source_code_hash  = filebase64sha256(data.archive_file.this.output_path)
}

resource "aws_api_gateway_rest_api" "this" {
  name = var.global_name
  endpoint_configuration {
    types = var.endpoint_configuration
  }
}

resource "aws_api_gateway_resource" "this" {
  rest_api_id = aws_api_gateway_rest_api.this.id
  parent_id   = aws_api_gateway_rest_api.this.root_resource_id
  path_part   = "api"
}

resource "aws_api_gateway_method" "this_GET" {
  rest_api_id   = aws_api_gateway_rest_api.this.id
  resource_id   = aws_api_gateway_resource.this.id
  http_method   = "GET"
  authorization = "NONE"
}

resource "aws_api_gateway_integration" "this_GET" {
  rest_api_id             = aws_api_gateway_rest_api.this.id
  resource_id             = aws_api_gateway_resource.this.id
  http_method             = "GET"
  integration_http_method = "POST"
  type                    = "AWS_PROXY"
  uri                     = aws_lambda_function.this.invoke_arn
}

resource "aws_lambda_permission" "this" {
  statement_id  = "AllowAPIGatewayInvoke"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.this.function_name
  principal     = "apigateway.amazonaws.com"
  source_arn    = "${aws_api_gateway_rest_api.this.execution_arn}/*/*/api"
}

resource "aws_api_gateway_method" "this_POST" {
  rest_api_id   = aws_api_gateway_rest_api.this.id
  resource_id   = aws_api_gateway_resource.this.id
  http_method   = "POST"
  authorization = "NONE"
}

resource "aws_api_gateway_integration" "this_POST" {
  rest_api_id             = aws_api_gateway_rest_api.this.id
  resource_id             = aws_api_gateway_resource.this.id
  http_method             = "POST"
  integration_http_method = "POST"
  type                    = "AWS_PROXY"
  uri                     = aws_lambda_function.this.invoke_arn
}

resource "aws_api_gateway_deployment" "this" {
  rest_api_id = aws_api_gateway_rest_api.this.id
}

resource "aws_api_gateway_stage" "this" {
  deployment_id = aws_api_gateway_deployment.this.id
  rest_api_id   = aws_api_gateway_rest_api.this.id
  stage_name    = "v1"
}