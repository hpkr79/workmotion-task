terraform {
  backend "s3" {
    bucket = "workmotion-task"
    key    = "terraform.tfstate"
    region = "eu-west-1"
  }

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "3.54.0"
    }
  }
}

provider "aws" {
  region = "eu-west-1"
}
