import json


def lambda_handler(event, context):
    response_message = {
        "Message": "Welcome to our demo API, here are the details of your request:",
        "Headers": event["headers"],
        "Method": event["httpMethod"],
        "Body": event["body"]
    }
    return {
        'statusCode': 200,
        'body': json.dumps(response_message),
        'headers': {"Access-Control-Allow-Origin": "*", "Content-Type": "application/json"}
    }
