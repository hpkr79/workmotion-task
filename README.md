## Steps

### Step 1 - Add following CI Variables in gitlab CI
##### AWS_ACCESS_KEY_ID
##### AWS_SECRET_ACCESS_KEY

### Step 2 - Create S3 bucket to store state file (if not created already)

### Step 3 - Update 'bucket' parameter in backend-provider.tf and change to bucket name where you want to keep the terraform state file

### Step 4 - Run Pipeline stages, init > validate > build > deploy
### Step 5 - Take API URL output from deploy stage and run following command and verify the output
```sh
curl --header "Content-Type: application/json" --data '{"username":"xyz","password":"xyz"}' ${API URL}/api
```
### Step 6 - Run cleanup stage in pipeline to clean the infra

### That's all folks !!